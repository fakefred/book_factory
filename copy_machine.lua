local function on_construct(pos)
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory()
    inv:set_size("src", 1)
    inv:set_size("blank", 1)
    inv:set_size("dst", 8)
    meta:set_string("formspec",
            "formspec_version[4]" ..
            "size[12,10]" ..
            "label[0.75,0.5;Book to copy]" ..
            "list[context;src;1,1;1,1;]" ..
            "label[2.5,0.5;Empty books]" ..
            "list[context;blank;2.75,1;1,1;]" ..
            "button[4.25,1;1.5,0.9;copy;Copy]" ..
            "list[context;dst;6.25,1;4,2;]" ..
            "list[current_player;main;1.2,4.25;8,1;]" ..
            "list[current_player;main;1.2,5.5;8,3;8]"
    )
end

local function allow_metadata_inventory_put(pos, listname, index, stack, player)
-- 	if minetest.is_protected(pos, player:get_player_name()) then
-- 		return 0
-- 	end
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	if listname == "src" then
        if stack:get_name() == "default:book_written" then
            return stack:get_count()
		else return 0 end
    elseif listname == "blank" then
        if stack:get_name() == "default:book" then
            return stack:get_count()
        else return 0 end
	elseif listname == "dst" then
		return 0
	end
end

local function allow_metadata_inventory_move(pos, from_list, from_index, to_list, to_index, count, player)
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local stack = inv:get_stack(from_list, from_index)
    return allow_metadata_inventory_put(pos, to_list, to_index, stack, player)
end   

local function on_receive_fields(pos, formname, fields, sender)
    if fields.quit then return end
    if fields.copy then
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        -- There is a book to copy to a greater-than-zero quantity of other books
        print(inv:get_stack("src", 1):get_name())
        print(inv:get_stack("blank", 1):get_name())
        if (inv:get_stack("src", 1):get_name() == "default:book_written" and 
           inv:get_stack("blank", 1):get_name() == "default:book" and
           inv:get_stack("blank", 1):get_count() > 0) then
            local src_stack = inv:get_stack("src", 1)
            local src_meta = src_stack:get_meta()
            local new_stack = ItemStack("default:book_written")
            local new_meta = new_stack:get_meta() -- ItemStackMetaRef object
            local blank_stack = inv:get_stack("blank", 1) -- gets a copy, not a reference
            -- Copy src book to one blank book
            -- Technically, copy the meta of the src default:book_written
            -- to a new default:book_written we just conjured into `new_stack`
            -- then eliminate a default:book from the `blank` list
            -- to create an illusion that we're copying a book to another as advertised
            new_meta:from_table(src_meta:to_table())
            inv:add_item("dst", new_stack)
            blank_stack:set_count(blank_stack:get_count() - 1)
            inv:set_stack("blank", 1, blank_stack)
        end
    end
end

minetest.register_node("book_factory:copy_machine", {
    description = "Copies content from one book to several others",
    tiles = {"book_factory_copy_machine.png"},
    groups = {cracky = 1, oddly_breakable_by_hand = 1},
    on_construct = on_construct,
    allow_metadata_inventory_put = allow_metadata_inventory_put,
    allow_metadata_inventory_move = allow_metadata_inventory_move,
    on_receive_fields = on_receive_fields
})
